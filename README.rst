Climate Data R Flexdashboard
============================

[[_TOC_]]

R Flexdashboard to help get an overview of the a years climate of WMO Region VI compared to a
reference period using information from gridded temperature and pressure field datasets,
aggregations and visualisations of DWD temperature and precipitation CLIMAT station data and
integration of DWD plots.

Screenshots
-----------

.. image:: https://gitlab.com/BENR0/climate_data_dashboard/-/raw/master/images/screen_1.png
   :width: 80
.. image:: https://gitlab.com/BENR0/climate_data_dashboard/-/raw/master/images/screen_2.png
   :width: 80
.. image:: https://gitlab.com/BENR0/climate_data_dashboard/-/raw/master/images/screen_3.png
   :width: 80
.. image:: https://gitlab.com/BENR0/climate_data_dashboard/-/raw/master/images/screen_4.png
   :width: 80
.. image:: https://gitlab.com/BENR0/climate_data_dashboard/-/raw/master/images/screen_5.png
   :width: 80

Usage/ Installation
-------------------

1. Download or git clone the repository.
2. Change into the R directory and run ``00_fetch_data.R`` on first usage:

   .. code-block:: R

       Rscript 00_fetch_data.R

   This will install all needed libraries and download the necessary data to run the
   dashboard.
3. In Rstudio open and run "dashboard.Rmd".


With the file ``dwd_climat_data_statistics.Rmd`` a report of aggregated Climat station data
grouped by subregions of WMO Region VI as used in the special issue [*State of the Climate*](
https://www.ametsoc.org/index.cfm/ams/publications/bulletin-of-the-american-meteorological-society-bams/state-of-the-climate/) in
Bulletin of the American Meteorological Society can be generated.
